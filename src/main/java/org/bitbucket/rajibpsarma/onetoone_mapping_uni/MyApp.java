package org.bitbucket.rajibpsarma.onetoone_mapping_uni;

import java.io.IOException;
import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/*
 @author: Rajib Sarma
 
 Need to create tables Address and Employee before executing.
 
create table address (
	id int IDENTITY(1,1) primary key,
	address varchar(50)
);
create table employee (
	id int IDENTITY(1,1) primary key,
	first_name varchar(25) not null,
	last_name varchar(25),
	address_id int not null,
	
	foreign key (address_id) references address(id)
);
 
 It prints the following (id will be different) to the console:
 
Creating an Employee and Address ...
Hibernate: insert into address (address) values (?)
Hibernate: insert into employee (address_id, first_name, last_name) values (?, ?, ?)

The details of the Employee is : 
9, Rajib Sarma, Flat no 123, Sobha Fantasy, Indiranagar

Updating Last Name and Address ...

The details of the Employee is : 
9, Rajib Prasad Sarma, Flat no 005, Sobha Fantasy, Bangalore

Deleting the employee ...

No Employee details found
Hibernate: delete from employee where id=?
Hibernate: delete from address where id=?
 */
public class MyApp {

	public static void main(String[] args) {
		Session session = null;

		try {
	    	Configuration cfg = new Configuration();
	    	
	    	// Configure using properties file
			Properties prop = new Properties();
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));
			cfg.setProperties(prop);
			
			// Map the Entities.
			cfg.addAnnotatedClass(Employee.class);
			cfg.addAnnotatedClass(EmployeeAddress.class);
	    	
			SessionFactory fac = cfg.buildSessionFactory();
			session = fac.openSession();
			
			session.beginTransaction();
			
			// Create employee and it's address
			System.out.println("Creating an Employee and Address ...");
			EmployeeAddress add = new EmployeeAddress("Flat no 123, Sobha Fantasy, Indiranagar");
			Employee emp = new Employee("Rajib", "Sarma");
			emp.setEmpAddress(add);
			session.save(emp);
			
			// Display the employee details
			displayEmployeeDetails(session, emp.getId());
			
			// Update the employee's address and last name
			System.out.println("\nUpdating Last Name and Address ...");
			Employee empNew = session.get(Employee.class, emp.getId());
			empNew.setLastName("Prasad Sarma");
			empNew.getEmpAddress().setAddress("Flat no 005, Sobha Fantasy, Bangalore");
			session.save(empNew);
			
			// Display the employee details again
			displayEmployeeDetails(session, empNew.getId());
			
			// Delete the employee details
			System.out.println("\nDeleting the employee ...");
			session.delete(empNew);
			
			// Display the employee details again
			displayEmployeeDetails(session, empNew.getId());
			
			
			session.getTransaction().commit();
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
		catch(Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		finally {
			if(session != null) {
				session.close();
			}
		}   		
	}
	
	private static void displayEmployeeDetails(Session session, int empId) {
		Employee emp = session.get(Employee.class, empId);
		if(emp == null) {
			System.out.println("\nNo Employee details found");
		}
		else {
			System.out.println("\nThe details of the Employee is : ");
			System.out.println(emp);
		}
	}

}
