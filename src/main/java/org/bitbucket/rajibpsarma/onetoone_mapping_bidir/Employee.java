package org.bitbucket.rajibpsarma.onetoone_mapping_bidir;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "employee")
	private EmployeeAddress empAddress;

	public Employee() {}
	
	public Employee(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public EmployeeAddress getEmpAddress() {
		return empAddress;
	}

	public void setEmpAddress(EmployeeAddress empAddress) {
		this.empAddress = empAddress;
	}
	
	public void setLastName(String lName) {
		lastName = lName;
	}

	public int getId() {
		return id;
	}
	
	public String toString() {
		StringBuffer str = new StringBuffer(id + ", " + firstName + " " + lastName);
		if(empAddress != null) {
			str.append(", " + empAddress.toString());
		}
		return str.toString();
	}
}