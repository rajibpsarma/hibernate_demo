package org.bitbucket.rajibpsarma.onetoone_mapping_bidir;

import java.io.IOException;
import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/*
 @author: Rajib Sarma
 
 Need to create tables Address and Employee before executing.
 
create table employee (
	id int IDENTITY(1,1) primary key,
	first_name varchar(25) not null,
	last_name varchar(25)
);

create table address (
	id int IDENTITY(1,1) primary key,
	emp_id int,
	street varchar(20),
	city varchar(20),
	state varchar(20),
	foreign key (emp_id) references employee(id)
);

 */
public class MyApp {

	public static void main(String[] args) {
		Session session = null;

		try {
	    	Configuration cfg = new Configuration();
	    	
	    	// Configure using properties file
			Properties prop = new Properties();
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));
			cfg.setProperties(prop);
			
			// Map the Entities.
			cfg.addAnnotatedClass(Employee.class);
			cfg.addAnnotatedClass(EmployeeAddress.class);
	    	
			SessionFactory fac = cfg.buildSessionFactory();
			session = fac.openSession();
			
			session.beginTransaction();
			
			// Create employee and it's address
			System.out.println("Creating an Employee and Address ...");
			EmployeeAddress add = new EmployeeAddress("House No C08, Gunjur", "Bangalore", "Karnataka");
			Employee emp = new Employee("Rajib", "Sarma");
			emp.setEmpAddress(add);
			add.setEmployee(emp);
			session.save(emp);
			
			// Display the employee details
			displayEmployeeDetails(session, emp.getId());
			
			// Update the employee's address and last name
			System.out.println("\nUpdating Last Name and Street ...");
			Employee empNew = session.get(Employee.class, emp.getId());
			empNew.setLastName("Prasad Sarma");
			empNew.getEmpAddress().setStreet("House No A23, Gunjur");
			session.save(empNew);
			
			// Display the employee details again
			displayEmployeeDetails(session, empNew.getId());
			
			// Delete the employee details
			System.out.println("\nDeleting the employee ...");
			session.delete(empNew);
			
			// Display the employee details again
			displayEmployeeDetails(session, empNew.getId());
			
			
			session.getTransaction().commit();
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
		catch(Exception e) {
			if(session != null)
				session.getTransaction().rollback();
			e.printStackTrace();
		}
		finally {
			if(session != null) {
				session.close();
			}
		}   		
	}
	
	private static void displayEmployeeDetails(Session session, int empId) {
		Employee emp = session.get(Employee.class, empId);
		if(emp == null) {
			System.out.println("\nNo Employee details found");
		}
		else {
			System.out.println("\nThe details of the Employee is : ");
			System.out.println(emp);
		}
	}

}
