package org.bitbucket.rajibpsarma.simple_mapping;

import java.util.List;
import java.util.Properties;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
/*
 * @author: Rajib Sarma
 */
public class MyApp {

    public static void main(String[] args) {
    	Session session = null;
    	
    	try {
        	Configuration cfg = new Configuration();
        	
        	// Configure using XML file
        	//cfg.configure("hibernate.cfg.xml");
        	
        	// Configure using properties file
			Properties prop = new Properties();
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));
			cfg.setProperties(prop);
			
			// Map the Entity. For XML config, it can also be done in the XML
			cfg.addAnnotatedClass(Student.class);
        	
    		SessionFactory fac = cfg.buildSessionFactory();
    		session = fac.openSession();
    		
    		session.beginTransaction();
    		
    		// Create 2 students
    		System.out.println("Creating 2 students ...");
    		Student st1 = new Student("Rajib", "Sarma", "rajib@raj.com");
    		Student st2 = new Student("Raju", "Srivastava", "raju@raj.com");
    		session.save(st1);
    		session.save(st2);
    		
    		// Display the created students
    		displayStudents(session);
    		
    		// Update a Student's last name
    		String lastName = "Prasad Sarma";
    		System.out.println("\nUpdating the last name to '" + lastName + "' ...");
    		st1.setLastName(lastName);
    		session.save(st1);
    		
    		// Display the students
    		displayStudents(session);
    		
    		// Update a Student Raju's email
    		String email = "raju.s@raj.com";
    		System.out.println("\nUpdating the email to '" + email + "'...");
    		Student st = session.get(Student.class, st2.getId());
    		st.setEmail(email);
    		session.saveOrUpdate(st);
    		
    		// Display the students
    		displayStudents(session);
    		
    		// Display first names
    		System.out.println("\nDisplaying the first names ...");
        	List<String> fNames = session.createQuery("select s.firstName from Student s", String.class).list();
        	fNames.forEach(student -> {
        		System.out.println(student);
        	});
        	
        	System.out.println("\nDisplaying the students using LIKE query '%Rajib%' ...");
        	//List<Student> students = session.createQuery("select s from Student s where s.firstName like '%Rajib%'", Student.class).list();
        	Query query = session.createQuery("select s from Student s where s.firstName like :fName");
        	query.setParameter("fName", "%Rajib%");
        	List<Student> students = query.list();
        	students.forEach(student -> {
        		System.out.println(student);
        	});
        	
        	System.out.println("\nDisplaying the students using Criteria '%Raj%' ...");
        	Criteria criteria = session.createCriteria(Student.class);
        	criteria.add(Restrictions.like("firstName", "%Raj%"));
        	students = criteria.list();
        	students.forEach(student -> {
        		System.out.println(student);
        	});
    		
    		// Delete the students
    		System.out.println("\nDeleting the students ...");
    		session.delete(st1);
    		// Another way
    		st = session.get(Student.class, st2.getId());
    		session.remove(st); // Can also use delete()
    		
    		// Display the students
    		//displayStudents(session);    		
    		
    		// Commit
    		session.getTransaction().commit();
    	}
		catch(Exception ex) {
			session.getTransaction().rollback();
			ex.printStackTrace();
		}
		finally {
			if(session != null) {
				session.close();
			}
		}    	

    }
    
    // Prints the students to the console
    private static void displayStudents(Session session) {
    	System.out.println("\nDisplaying the students ...");
    	List<Student> students = session.createQuery("from Student", Student.class).list();
    	students.forEach(student -> {
    		System.out.println(student);
    	});
    }

}
