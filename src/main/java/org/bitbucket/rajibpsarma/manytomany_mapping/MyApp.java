package org.bitbucket.rajibpsarma.manytomany_mapping;

import java.io.IOException;
import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/*
 @author: Rajib Sarma
 
 Need to create the following tables before executing.
 
	create table Cart (
		id int IDENTITY(1,1) primary key,
		name varchar(25) not null
	);
	create table Item (
		id int IDENTITY(1,1) primary key,
		name varchar(20),
		price decimal(10,2)
	);
	create table Lk_Cart_Item (
		cart_id int,
		item_id int,
		quantity int,
		primary key (cart_id, item_id)
	);

 It prints the following (id will be different) to the console:
 
	Creating Carts and Items ...
	Hibernate: insert into Cart (name) values (?)
	Hibernate: insert into Item (name, price) values (?, ?)
	Hibernate: insert into Item (name, price) values (?, ?)
	Hibernate: insert into Cart (name) values (?)
	Hibernate: insert into Item (name, price) values (?, ?)
	
	The Cart #11, 'My Cart 1' contains :
	#19, Item 1, Rs. 100.11
	#20, Item 2, Rs. 200.22
	
	The Cart #12, 'My Cart 2' contains :
	#20, Item 2, Rs. 200.22
	#21, Item 3, Rs. 300.33
	
	Updating Carts ...
	Hibernate: insert into Item (name, price) values (?, ?)
	Hibernate: insert into Lk_Cart_Item (cart_id, item_id) values (?, ?)
	Hibernate: insert into Lk_Cart_Item (cart_id, item_id) values (?, ?)
	Hibernate: insert into Lk_Cart_Item (cart_id, item_id) values (?, ?)
	Hibernate: insert into Lk_Cart_Item (cart_id, item_id) values (?, ?)
	Hibernate: insert into Lk_Cart_Item (cart_id, item_id) values (?, ?)
	Hibernate: insert into Lk_Cart_Item (cart_id, item_id) values (?, ?)
	
	The Cart #11, 'My Cart 1' contains :
	#19, Item 1, Rs. 100.11
	#20, Item 2, Rs. 200.22
	#22, Item 4, Rs. 400.44
	
	The Cart #12, 'My Cart 2' contains :
	#20, Item 2, Rs. 200.22
	#21, Item 3, Rs. 300.33
	#22, Item 4, Rs. 400.44
	
	Deleting the Carts and items ...
	
	No Cart details found
	
	No Cart details found
	Hibernate: delete from Lk_Cart_Item where cart_id=?
	Hibernate: delete from Lk_Cart_Item where cart_id=?
	Hibernate: delete from Item where id=?
	Hibernate: delete from Item where id=?
	Hibernate: delete from Item where id=?
	Hibernate: delete from Cart where id=?
	Hibernate: delete from Item where id=?
	Hibernate: delete from Cart where id=?

 Before deleting, the database tables contains values such as:
	
	Table: Cart
	id	name
	7	My Cart 1
	8	My Cart 2
	
	Table: Item
	id	name	price
	11	Item 1	100.11
	12	Item 2	200.22
	13	Item 3	300.33
	14	Item 4	400.44
	
	Table: Lk_Cart_Item
	cart_id	item_id	
	7		11	
	7		12	
	7		14	
	8		12	
	8		13	
	8		14	
 */
public class MyApp {

	public static void main(String[] args) {
		Session session = null;

		try {
	    	Configuration cfg = new Configuration();
	    	
	    	// Configure using properties file
			Properties prop = new Properties();
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));
			cfg.setProperties(prop);
			
			// Map the Entities.
			cfg.addAnnotatedClass(Cart.class);
			cfg.addAnnotatedClass(Item.class);
	    	
			SessionFactory fac = cfg.buildSessionFactory();
			session = fac.openSession();
			
			session.beginTransaction();
			
			// Create carts and items
			System.out.println("Creating Carts and Items ...");
			Cart cart1 = new Cart("My Cart 1");
			Cart cart2 = new Cart("My Cart 2");
			Item item1 = new Item("Item 1", 100.11F);
			Item item2 = new Item("Item 2", 200.22F);
			Item item3 = new Item("Item 3", 300.33F);
			cart1.addItem(item1);
			cart1.addItem(item2);
			cart2.addItem(item2);
			cart2.addItem(item3);
			session.persist(cart1);
			session.persist(cart2);
					
			// Display the cart details
			displayCartDetails(session, cart1.getId());
			displayCartDetails(session, cart2.getId());
			
			// Update carts to add items
			System.out.println("\nUpdating Carts ...");
			cart1 = session.get(Cart.class, cart1.getId());
			Item item4 = new Item("Item 4", 400.44F);
			cart1.addItem(item4);
			cart2.addItem(item4);
			session.persist(cart1);
			session.persist(cart2);
			session.flush();
			
			// Display the cart details
			displayCartDetails(session, cart1.getId());
			displayCartDetails(session, cart2.getId());
			
			// Delete the Cart details
			System.out.println("\nDeleting the Carts and items ...");
			session.delete(cart1);
			session.delete(cart2);
			
			// Display the cart details
			displayCartDetails(session, cart1.getId());
			displayCartDetails(session, cart2.getId());
			
			session.getTransaction().commit();
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
		catch(Exception e) {
			if(session != null)
				session.getTransaction().rollback();
			e.printStackTrace();
		}
		finally {
			if(session != null) {
				session.close();
			}
		}   		
	}
	
	private static void displayCartDetails(Session session, int id) {
		Cart p = session.get(Cart.class, id);
		if(p == null) {
			System.out.println("\nNo Cart details found");
		}
		else {
			System.out.println("\n" + p);
		}
	}

}
