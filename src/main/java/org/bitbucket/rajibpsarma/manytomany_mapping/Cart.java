package org.bitbucket.rajibpsarma.manytomany_mapping;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="Cart")
public class Cart {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name = "Lk_Cart_Item",
		joinColumns = {@JoinColumn(name = "cart_id")},
		inverseJoinColumns = { @JoinColumn(name = "item_id") }
	)
	List<Item> items = new ArrayList<Item>();
	
	public Cart() {}

	public Cart(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public List<Item> getItems() {
		return items;
	}

	public void addItem(Item item) {
		this.items.add(item);
	}
	
	public String toString() {
		StringBuffer buf = new StringBuffer("The Cart #" + id + ", '" + name + "' contains :");
		if(!items.isEmpty()) {
			items.forEach((item) -> {
				buf.append(item.toString());
			});
		}
		else {
			buf.append("No Items !");
		}
		return buf.toString();
	}
}