package org.bitbucket.rajibpsarma.manytomany_mapping;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="Item")
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private int id;
	private String name;
	private float price;
	
	@ManyToMany(mappedBy = "items")
	private List<Cart> carts = new ArrayList<Cart>();
	
	public Item() {}

	public Item(String name, float price) {
		this.name = name;
		this.price = price;
	}

	public List<Cart> getCarts() {
		return carts;
	}

	public void addCart(Cart cart) {
		this.carts.add(cart);
	}
	
	public String toString() {
		return "\n#" + id + ", " + name + ", Rs. " + price;
	}
}
