package org.bitbucket.rajibpsarma.onetomany_mapping_bidir;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/*
 @author: Rajib Sarma
 
 Need to create the following tables before executing.
 
	create table Person (
		id int IDENTITY(1,1) primary key,
		first_name varchar(25) not null,
		last_name varchar(25)
	);
	
	create table Address (
		id int IDENTITY(1,1) primary key,
		person_id int,
		address_type varchar(20),
		street varchar(20),
		city varchar(20),
		state varchar(20),
		foreign key (person_id) references Person(id)
	);

 
 It prints the following (id will be different) to the console:
 
	Creating a Person and Address ...
	Hibernate: insert into Person (first_name, last_name) values (?, ?)
	Hibernate: insert into Address (address_type, city, person_id, state, street) values (?, ?, ?, ?, ?)
	Hibernate: insert into Address (address_type, city, person_id, state, street) values (?, ?, ?, ?, ?)
	
	The details of the Person is : 
	11, Rajib Sarma
	Home Address: #86, Lakshmi Nilaya Bangalore Karnataka
	Office Address: #76, Whitefield Road Bangalore Karnataka
	
	Updating and adding Last Name and address ...
	Hibernate: insert into Address (address_type, city, person_id, state, street) values (?, ?, ?, ?, ?)
	Hibernate: update Person set first_name=?, last_name=? where id=?
	Hibernate: update Address set address_type=?, city=?, person_id=?, state=?, street=? where id=?
	
	The details of the Person is : 
	11, Rajib Prasad Sarma
	Home Address: #403, Niswarga Dham Bangalore Karnataka
	Office Address: #76, Whitefield Road Bangalore Karnataka
	Temporary Address: 4th Cross, 7th Main Bangalore Karnataka
	
	Deleting the person and addresses ...
	
	No Person details found
	Hibernate: delete from Address where id=?
	Hibernate: delete from Address where id=?
	Hibernate: delete from Address where id=?
	Hibernate: delete from Person where id=? 

 Before deleting, the database tables contains values such as:

	Table: Person
	id	first_name	last_name
	11	Rajib		Prasad Sarma
	
	Table: Address
	id	person_id	address_type	street					city		state
	16	11			Home			#403, Niswarga Dham		Bangalore	Karnataka
	17	11			Office			#76, Whitefield Road	Bangalore	Karnataka
	18	11			Temporary		4th Cross, 7th Main		Bangalore	Karnataka
 */
public class MyApp {

	public static void main(String[] args) {
		Session session = null;

		try {
	    	Configuration cfg = new Configuration();
	    	
	    	// Configure using properties file
			Properties prop = new Properties();
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));
			cfg.setProperties(prop);
			
			// Map the Entities.
			cfg.addAnnotatedClass(Person.class);
			cfg.addAnnotatedClass(Address.class);
	    	
			SessionFactory fac = cfg.buildSessionFactory();
			session = fac.openSession();
			
			session.beginTransaction();
			
			// Create Person and it's address
			System.out.println("Creating a Person and Address ...");
			Address add1 = new Address("Home", "#86, Lakshmi Nilaya", "Bangalore", "Karnataka");
			Address add2 = new Address("Office", "#76, Whitefield Road", "Bangalore", "Karnataka");
			//session.save(add1);
			//session.save(add2);
			Person p = new Person("Rajib", "Sarma");
			p.addAddress(add1);
			p.addAddress(add2);
			add1.setPerson(p);
			add2.setPerson(p);
			session.save(p);
			
			// Display the person details
			displayPersonDetails(session, p.getId());
			
			// Update the person's home address, add a temp address and update last name
			System.out.println("\nUpdating and adding Last Name and address ...");
			Person pNew = session.get(Person.class, p.getId());
			pNew.setLastName("Prasad Sarma");
			List<Address> addreses = pNew.getAddresses();
			addreses.forEach((add) -> {
				// Update home address
				if("HOME".equalsIgnoreCase(add.getAddressType())) {
					add.setStreet("#403, Niswarga Dham"); 
				}
			});
			// add temp add
			Address temp = new Address("Temporary", "4th Cross, 7th Main", "Bangalore", "Karnataka");
			temp.setPerson(pNew);
			addreses.add(temp);
			session.save(pNew);
			session.flush();
			
			// Display the person details
			displayPersonDetails(session, pNew.getId());
			
			// Delete the person details
			System.out.println("\nDeleting the person and addresses ...");
			Person person = session.get(Person.class, pNew.getId());
			session.delete(person);
			
			// Display the person details
			displayPersonDetails(session, p.getId());
			
			session.getTransaction().commit();
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
		catch(Exception e) {
			if(session != null)
				session.getTransaction().rollback();
			e.printStackTrace();
		}
		finally {
			if(session != null) {
				session.close();
			}
		}   		
	}
	
	private static void displayPersonDetails(Session session, int id) {
		Person p = session.get(Person.class, id);
		if(p == null) {
			System.out.println("\nNo Person details found");
		}
		else {
			System.out.println("\nThe details of the Person is : ");
			System.out.println(p);
		}
	}

}
