package org.bitbucket.rajibpsarma.onetomany_mapping_bidir;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author: Rajib Sarma
 */

@Entity
@Table(name = "Person")
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "person")
	private List<Address> addresses = new ArrayList<Address>();

	public Person() {}
	
	public Person(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public void addAddress(Address address) {
		this.addresses.add(address);
	}
	
	public List<Address> getAddresses() {
		return addresses;
	}
	
	public void setLastName(String lName) {
		lastName = lName;
	}

	public int getId() {
		return id;
	}
	
	public String toString() {
		StringBuffer str = new StringBuffer(id + ", " + firstName + " " + lastName);
		if(addresses != null && !addresses.isEmpty()) {
			addresses.forEach((add) -> {
				str.append("\n" + add.toString());
			}); 
		}
		return str.toString();
	}
}