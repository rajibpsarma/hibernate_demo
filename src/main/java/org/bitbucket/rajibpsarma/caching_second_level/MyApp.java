package org.bitbucket.rajibpsarma.caching_second_level;

import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
/*
  We are experimenting with Second level of hibernate caching here.
  We need the following table to be created:
  
	create table student (
		id int IDENTITY(1,1) primary key,
		first_name varchar(25) not null,
		last_name varchar(25),
		email varchar(25)
	);  
  
  It prints something like the following to the console, when executed:

	This example does not work. It gives warning:
	WARN: HHH020100: The Ehcache second-level cache provider for Hibernate is deprecated.
	Need to investigate
  
  @author: Rajib Sarma
 */
public class MyApp {

    public static void main(String[] args) {
    	Session session = null;
    	
    	try {
        	Configuration cfg = new Configuration();
         	
        	// Configure using properties file
			Properties prop = new Properties();
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("second_level_cache/hibernate.properties"));
			cfg.setProperties(prop);
			
			// Map the Entity.
			cfg.addAnnotatedClass(Student.class);
        	
    		SessionFactory fac = cfg.buildSessionFactory();
    		session = fac.openSession();
    		
    		session.beginTransaction();
    		
    		// Create 2 students
    		System.out.println("Creating 2 students ...");
    		Student st1 = new Student("Rajib", "Sarma", "rajib@raj.com");
    		Student st2 = new Student("Raju", "Srivastava", "raju@raj.com");
    		session.save(st1);
    		session.persist(st2);
    		
    		// Display the created students
    		// it should not generate any SQL
    		System.out.println("\nDisplaying the students from session cache ...");
    		System.out.println(session.get(Student.class, st1.getId()));
    		System.out.println(session.get(Student.class, st2.getId()));
    		session.get(Student.class, st1.getId());
    		
    		
    		// Display the students again
    		// it should not generate any SQL
    		System.out.println("\nDisplaying the students from session cache ...");
    		System.out.println(session.get(Student.class, st1.getId()));
    		System.out.println(session.get(Student.class, st2.getId()));
    		
    		session.clear();
    		// Display the students again
    		// it now should generate the SQL, it's not in cache.
    		System.out.println("\nDisplaying the students after clearing session ...");
    		st1 = session.get(Student.class, st1.getId());
    		System.out.println(st1);
    		System.out.println(session.get(Student.class, st2.getId()));
    		
    		// Commit
    		session.getTransaction().commit();
    		session.close();
    		
    		// Now, open a new session and get the student, it will generate the SQL
    		System.out.println("\nFetching the student in a new session, generates the SQL");
    		Session session2 = fac.openSession();
    		session2.beginTransaction();
    		st1 = session2.get(Student.class, st1.getId());
    		System.out.println(st1);
    		st2 = session2.get(Student.class, st2.getId());
    		System.out.println(st2);
    		
    		// Delete the students
    		System.out.println("\nDeleting the students ...");
    		session2.delete(st1);
    		// Another way
    		Student st = session2.get(Student.class, st2.getId());
    		session2.remove(st); // Can also use delete()
    		
    		session2.getTransaction().commit();
    		session2.close();
    	}
		catch(Exception ex) {
			ex.printStackTrace();
		}
    }
}