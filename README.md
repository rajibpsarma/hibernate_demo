# Hibernate sample examples #

It contains Hibernate examples written using Java and uses SQL Server database.

##### The various examples are: #####
* **Simple Mapping**: Look into package [org.bitbucket.rajibpsarma.simple_mapping](https://bitbucket.org/rajibpsarma/hibernate_demo/src/master/src/main/java/org/bitbucket/rajibpsarma/simple_mapping/).
The table is created using SQL:

>
	create table student (
		id int IDENTITY(1,1) primary key,
		first_name varchar(25) not null,
		last_name varchar(25),
		email varchar(25)
	);
	
* **One-to-One Mapping Unidirectional**: Look into package [org.bitbucket.rajibpsarma.onetoone_mapping_uni](https://bitbucket.org/rajibpsarma/hibernate_demo/src/master/src/main/java/org/bitbucket/rajibpsarma/onetoone_mapping_uni/).
The tables are created using SQL:

>
	create table address (
		id int IDENTITY(1,1) primary key,
		address varchar(50)
	);
	create table employee (
		id int IDENTITY(1,1) primary key,
		first_name varchar(25) not null,
		last_name varchar(25),
		address_id int not null,
		foreign key (address_id) references address(id)
	);
	
* **One-to-One Mapping Bidirectional**: Look into package [org.bitbucket.rajibpsarma.onetoone_mapping_bidir](https://bitbucket.org/rajibpsarma/hibernate_demo/src/master/src/main/java/org/bitbucket/rajibpsarma/onetoone_mapping_bidir/).
The tables are created using SQL:

>
	create table employee (
		id int IDENTITY(1,1) primary key,
		first_name varchar(25) not null,
		last_name varchar(25)
	);
	create table address (
		id int IDENTITY(1,1) primary key,
		emp_id int,
		street varchar(20),
		city varchar(20),
		state varchar(20),
		foreign key (emp_id) references employee(id)
	);
	

* **One-to-Many Mapping Bidirectional**: Look into package [org.bitbucket.rajibpsarma.onetomany_mapping_bidir](https://bitbucket.org/rajibpsarma/hibernate_demo/src/master/src/main/java/org/bitbucket/rajibpsarma/onetomany_mapping_bidir/).
The tables are created using SQL:

>
	create table Person (
		id int IDENTITY(1,1) primary key,
		first_name varchar(25) not null,
		last_name varchar(25)
	);
	create table Address (
		id int IDENTITY(1,1) primary key,
		person_id int,
		address_type varchar(20),
		street varchar(20),
		city varchar(20),
		state varchar(20),
		foreign key (person_id) references Person(id)
	);
	

* **Many-to-Many Mapping**: Look into package [org.bitbucket.rajibpsarma.manytomany_mapping](https://bitbucket.org/rajibpsarma/hibernate_demo/src/master/src/main/java/org/bitbucket/rajibpsarma/manytomany_mapping/).
The tables are created using SQL:

>
	create table Cart (
		id int IDENTITY(1,1) primary key,
		name varchar(25) not null
	);
	create table Item (
		id int IDENTITY(1,1) primary key,
		name varchar(20),
		price decimal(10,2)
	);
	create table Lk_Cart_Item (
		cart_id int,
		item_id int,
		quantity int,
		primary key (cart_id, item_id)
	);
	
* **First Level Cache** related example is here: [org.bitbucket.rajibpsarma.caching_first_level](https://bitbucket.org/rajibpsarma/hibernate_demo/src/master/src/main/java/org/bitbucket/rajibpsarma/caching_first_level/)

* **Second Level Cache** related example is here: [org.bitbucket.rajibpsarma.caching_second_level](https://bitbucket.org/rajibpsarma/hibernate_demo/src/master/src/main/java/org/bitbucket/rajibpsarma/caching_second_level/)